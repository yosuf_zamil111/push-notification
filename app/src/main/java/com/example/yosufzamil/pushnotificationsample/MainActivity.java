package com.example.yosufzamil.pushnotificationsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements ValueEventListener{

    TextView title,message;
    private TextView heading_text;
    private EditText input_text;
    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference mRootReference = firebaseDatabase.getReference();
    private DatabaseReference mHeadingReference = mRootReference.child("heading");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = (TextView) findViewById(R.id.msg_title);
        message = (TextView) findViewById(R.id.msg_message);

        heading_text = (TextView) findViewById(R.id.headingText);
        input_text = (EditText) findViewById(R.id.headingInput);


        /*if(getIntent().getExtras() == null){

            for(String key : getIntent().getExtras().keySet()){

                if(key.equals("title"))
                    title.setText(getIntent().getExtras().getString(key));

                else if(key.equals("message"))
                    message.setText(getIntent().getExtras().getString(key));
            }
        }*/
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        if(dataSnapshot.getValue(String.class)!= null){

            String key = dataSnapshot.getKey();

            if(key.equals("heading")){

                String heading = dataSnapshot.getValue(String.class);
                heading_text.setText(heading);

            }

        }

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        mHeadingReference.addValueEventListener(this);
    }

    public void submitHeading(View view) {

        String heading = input_text.getText().toString();
        mHeadingReference.setValue(heading);
        input_text.setText("");

    }
}
